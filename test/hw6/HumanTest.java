package hw6;

import com.company.homeworks.homework6.entity.Family;
import com.company.homeworks.homework6.entity.Human;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void setFamilyTest() {
        Human father = new Human();
        Family family = new Family(father, new Human());

        assertEquals(family, father.getFamily());
    }


    @Test
    void testToString() {
        Human human = new Human("Vadim", "Tartakovsky", (short) 2003);

        assertEquals(human.toString(), "\nHuman{" +
                "\nname='" + "Vadim" + '\'' +
                ", \nsurname='" + "Tartakovsky" + '\'' +
                ", \nyear=" + "2003" +
                ", \niq=" + "0" +
                ", \nschedule=" + "[[null, null], [null, null], [null, null], [null, null], [null, null], [null, null], [null, null]]" +
                '}');
    }
}