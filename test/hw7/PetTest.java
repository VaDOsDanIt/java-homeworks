package hw7;

import com.company.homeworks.homework7.entity.DomesticCat;
import com.company.homeworks.homework7.entity.Pet;
import com.company.homeworks.homework7.enums.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PetTest {

    @Test
    void testToString() {
        Pet pet = new DomesticCat("boss", (short) 5, (short) 57, new String[]{"habit 1"});

        assertEquals(pet.toString(),  "Pet{" +
                "species=" + Species.DOMESTIC_CAT +
                ", nickname='" + "boss" + '\'' +
                ", age=" + "5" +
                ", trickLevel=" + "57" +
                ", habits=" + "[habit 1]" +
                '}');

    }
}