package com.company.homeworks.homework9.entity;

import com.company.homeworks.homework9.entity.Family;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private short year;
    private short iq;
    private Human mother;
    private Human father;
    private Map<String, String> schedule = new HashMap<>();
    private Family family;

    public void greetPet() {
        System.out.println("Привет, " + this.family.getPet().iterator().next().getNickname());
    }

    public void describePet() {
        System.out.println("У меня есть " + this.family.getPet().iterator().next().getSpecies() + ", ему " + this.family.getPet().iterator().next().getAge() + " лет, он " + (this.family.getPet().iterator().next().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public short getIq() {
        return iq;
    }

    public void setIq(short iq) {
        this.iq = iq;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "\nHuman{" +
                "\nname='" + name + '\'' +
                ", \nsurname='" + surname + '\'' +
                ", \nyear=" + year +
                ", \niq=" + iq +
                ", \nschedule=" + schedule.toString() +
                '}';
    }

    public Human(String name, String surname, short year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, short year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(mother, human.mother) &&
                Objects.equals(father, human.father) &&
                Objects.equals(schedule, human.schedule) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, mother, father, schedule, family);
    }

    public Human(String name, String surname, short year, short iq, Human mother, Human father, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human() {
    }

    @Override
    protected void finalize ( ) {
        System.out.println("Удаляется обьект - " + this);
    }
}
