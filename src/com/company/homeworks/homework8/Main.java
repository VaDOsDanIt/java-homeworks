package com.company.homeworks.homework8;

import com.company.homeworks.homework7.entity.*;
import com.company.homeworks.homework7.enums.DayOfWeek;
import com.company.homeworks.homework7.enums.Species;

public class Main {

    public static void main(String[] args) {

        Human father = new Man("Петр", "Петров", (short) 1996);
        Human mother = new Woman("Ульяна", "Петрова", (short) 1997);
        Human child = new Man("Вадим", "Петров", (short) 2010, mother, father);

        String[] petHabitsArray = {"1 habit", "2 habit"};

        Pet pet = new Fish("Рібка");


        Family family = new Family(mother, father);

        family.addChild(child);

        pet.respond();

        family.addChild(child);
        family.deleteChild(0);
        family.addChild(child);


        System.out.println(family);
    }
}
