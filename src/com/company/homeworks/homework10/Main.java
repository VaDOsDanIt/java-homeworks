package com.company.homeworks.homework10;

import com.company.homeworks.homework10.controller.FamilyController;
import com.company.homeworks.homework10.entity.*;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();

        Human vadim = new Man("Vadim", "Tartakovsky", "14/05/2003");
        Human uliana = new Woman("Uliana", "Kovalenko", "13/11/2003");
        Human adoptChild = new Man("Oleg", "Petrov", "15/7/2008");

        Family family = familyController.createNewFamily(uliana, vadim);

        System.out.println(family.getFather().describeAge());

//        familyController.bornChild(family, "Vlad", "Vladislava");

//        familyController.adoptChild(family, adoptChild);

//        familyController.displayAllFamilies();
//        System.out.println(familyController.getAllFamilies());;

//        familyController.getFamiliesBiggerThan(5);
//        familyController.getFamiliesBiggerThan(2);

//        familyController.getFamiliesLessThan(2);
//        familyController.getFamiliesLessThan(5);

//        System.out.println(familyController.countFamiliesWithMemberNumber(4));
//        System.out.println(familyController.countFamiliesWithMemberNumber(3));

//        familyController.deleteFamilyByIndex(0);
//        familyController.deleteFamilyByIndex(1);

//        familyController.deleteAllChildrenOlderThan(9);

//        System.out.println(familyController.count());

//        familyController.addPet(0, new RoboCat("iCat"));
//        familyController.addPet(0, new Dog("iDog"));
//        System.out.println(familyController.getPets(0));

    }
}
