package com.company.homeworks.homework7;

import com.company.homeworks.homework7.entity.*;
import com.company.homeworks.homework7.enums.DayOfWeek;
import com.company.homeworks.homework7.enums.Species;

public class Main {

    public static void main(String[] args) {

        Human father = new Man("Петр", "Петров", (short) 1996);
        Human mother = new Woman("Ульяна", "Петрова", (short) 1997);
        Human child = new Man("Вадим", "Петров", (short) 2010, mother, father);

        String[] petHabitsArray = {"1 habit", "2 habit"};

        Pet pet = new Fish("Рібка");

        String[][] schedule = new String[][]{{DayOfWeek.MONDAY.toString(), "Задача на понедельник"},
                {DayOfWeek.TUESDAY.name(), "Задача на вторник"},
                {DayOfWeek.WEDNESDAY.name(), "Задача на среду"},
                {DayOfWeek.THURSDAY.name(), "Задача на четверг"},
                {DayOfWeek.FRIDAY.name(), "Задача на пятницу"},
                {DayOfWeek.SATURDAY.name(), "Задача на субботу"},
                {DayOfWeek.SUNDAY.name(), "Задача на воскресенье"}};


        Family family = new Family(mother, father);

        family.addChild(child);

        pet.respond();

        System.out.println(family);
    }
}
