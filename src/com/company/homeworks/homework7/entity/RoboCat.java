package com.company.homeworks.homework7.entity;

import com.company.homeworks.homework7.enums.Species;

import java.util.Arrays;

public class RoboCat extends Pet implements Foulable {
    public RoboCat(String nickname) {
        super(nickname);
        this.setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname, short age, short trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.ROBOCAT);
    }

    public RoboCat() {
        this.setSpecies(Species.ROBOCAT);
    }

    @Override
    public void foul() {
        System.out.println("Здравствуйте, собственно говоря...");
    }

    @Override
    public void respond() {
        System.out.println("efowfnosoi".getBytes().toString());
    }
}
