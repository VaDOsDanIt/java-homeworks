package com.company.homeworks.homework7.entity;

import com.company.homeworks.homework7.enums.Species;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private short age;
    private short trickLevel;
    private String[] habits = new String[20];

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public abstract void respond();

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public short getAge() {
        return age;
    }

    public short getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }


    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, short age, short trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    @Override
    protected void finalize ( ) {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species=" + species +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
