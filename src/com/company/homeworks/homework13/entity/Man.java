package com.company.homeworks.homework13.entity;

import java.util.Map;

public final class Man extends Human {
    private static final long serialVersionUID = 12323534646433565L;

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, Human mother, Human father) {
        super(name, surname, birthDate, mother, father);
    }

    public Man(String name, String surname, String birthDate, short iq, Human mother, Human father, Map<String, String> schedule) {
        super(name, surname, birthDate, iq, mother, father, schedule);
    }

    public Man() {
    }

    @Override
    public void greetPet() {
        if (this.getFamily().getPet() != null) {
            System.out.println("MEEEAAAOUUUU, " + this.getFamily().getPet().iterator().next().getNickname());
        } else {
            System.out.println("В Вашей семье нет питомца! :(");
        }
    }

    public void repairCar() {
        System.out.println("Мужик сказал - мужик сломал.");
    }
}
