package com.company.homeworks.homework13.entity;

import com.company.homeworks.homework13.enums.Species;

import java.util.Set;

public class Fish extends Pet {
    private static final long serialVersionUID = 1232353435643565L;

    public Fish(String nickname) {
        super(nickname);
        this.setSpecies(Species.FISH);
    }

    public Fish(String nickname, short age, short trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.FISH);
    }

    public Fish() {
        this.setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("Буль буль, буль буль");
    }
}
