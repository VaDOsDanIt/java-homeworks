package com.company.homeworks.homework13.entity;

import com.company.homeworks.homework13.enums.Species;

import java.util.Set;

public class Dog extends Pet implements Foulable {
    private static final long serialVersionUID = 12323534643532565L;

    public Dog(String nickname) {
        super(nickname);
        this.setSpecies(Species.DOG);
    }

    public Dog(String nickname, short age, short trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.DOG);
    }

    public Dog() {
        this.setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Здравствуйте, собственно говоря...");
    }

    @Override
    public void foul() {
        System.out.println("Нужно замести следы...");
    }
}
