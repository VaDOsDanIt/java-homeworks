package com.company.homeworks.homework13.controller;

import com.company.homeworks.homework13.entity.Family;
import com.company.homeworks.homework13.entity.Human;
import com.company.homeworks.homework13.entity.Pet;
import com.company.homeworks.homework13.exceptions.FamilyOverflowException;
import com.company.homeworks.homework13.service.FamilyService;

import java.util.List;

public class FamilyController {
    FamilyService services = new FamilyService();

    public Family getFamilyByIndex(int index) {
        return services.getFamilyByIndex(index);
    }

    public void displayAllFamilies() {
        services.displayAllFamilies();
    }

    public List<Family> getAllFamilies() {
        return services.getAllFamilies();
    }

    public void getFamiliesBiggerThan(int countPerson) {
        services.getFamiliesBiggerThan(countPerson);
    }

    public void getFamiliesLessThan(int countPerson) {
        services.getFamiliesLessThan(countPerson);
    }

    public int countFamiliesWithMemberNumber(int countPerson) {
        return services.countFamiliesWithMemberNumber(countPerson);
    }


    public Family createNewFamily(Human firstPerson, Human secondPerson) {
        return services.createNewFamily(firstPerson, secondPerson);
    }

    public Family bornChild(Family family, String nameBoy, String nameGirl) {
        if (family != null) {
            try {
                return services.bornChild(family, nameBoy, nameGirl);
            } catch (FamilyOverflowException e) {
                System.out.println(e.getMessage());
            }
        }
        return null;
    }

    public Family adoptChild(Family family, Human child) {
        if (family != null) {
            try {
                return services.adoptChild(family, child);
            } catch (FamilyOverflowException e) {
                System.out.println(e.getMessage());
            }
        }
        return null;
    }

    public void deleteAllChildrenOlderThan(int age) {
        services.deleteAllChildrenOlderThan(age);
    }

    public boolean deleteFamilyByIndex(int index) {
        return services.deleteFamilyByIndex(index);
    }

    public int count() {
        return services.count();
    }

    public List<Pet> getPets(int indexFamily) {
        return services.getPets(indexFamily);
    }

    public Family addPet(int indexFamily, Pet pet) {
        return services.addPet(indexFamily, pet);
    }

    public boolean saveData() {
        return services.saveData();
    }

    public void loadData() {
        services.loadData();
    }

}
