package com.company.homeworks;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;


public class homework1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();

        int prevUserNumbersArrayEmptyPosition = 0;
        int countTry = 0;
        int[] prevUserNumbers = new int[100];

        final int maxNumber = 100, minNumber = 0;
        final int diff = maxNumber - minNumber;

        String userName = null;
        int cmd = 0;
        do {
            System.out.print("\033[H\033[2J");
            System.out.flush();

            System.out.println("\n\tНачните игру" + (userName != null ? ", " + userName + "!" : "!"));
            if (userName == null) {
                System.out.print("\tВведите своё имя: ");
                userName = scan.next();
            }

            System.out.println("\n\t1) Начать игру.");
            System.out.println("\t2) Изменить имя.");
            System.out.println("\t0) Выход.");

            System.out.print("\tВведите код действия: ");
            while (!scan.hasNextInt()) {
                System.out.println("\tОшибка действия!");
                scan.next();
                System.out.print("\n\tВведите код действия: ");
            }
            cmd = scan.nextInt();

            switch (cmd) {
                case 1:
                    int randomNumber = rand.nextInt(diff + 1);
                    int userNumber = -1;

                    System.out.println("\n\tМы загадали случайное число от " + minNumber + " до " + maxNumber + ", попробуй его угадать!");
                    do {
                        System.out.print("\n\tВведите число: ");
                        while (!scan.hasNextInt()) {
                            System.out.println("\tВы ввели не число!");
                            scan.next();
                            System.out.print("\n\tВведите число: ");
                        }
                        userNumber = scan.nextInt();

                        prevUserNumbers[prevUserNumbersArrayEmptyPosition++] = userNumber;
                        countTry++;
                        System.out.println(checkNumber(userNumber, randomNumber, userName, prevUserNumbers, countTry));
                    } while (randomNumber != userNumber);
                    break;
                case 2:
                    System.out.println("\n\tСмена имени пользователя.");
                    System.out.print("\n\tВведите новое имя: ");
                    userName = scan.next();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("\n\tОшибка действия!");
                    scan.nextLine();
                    break;
            }

        } while (cmd != 0);

    }

    public static String checkNumber(int userNumber, int randomNumber, String userName, int[] prevUserNumbers, int countTry) {
        if (userNumber > randomNumber) {
            return "\tВаше число слишком большое!";
        } else if (userNumber < randomNumber) {
            return "\tВаше число слишком маленькое!";
        } else {
            System.out.println("\n\tВаши числа: " + Arrays.toString(Arrays.copyOf(prevUserNumbers, countTry)));
            return "\t" + userName + ", вы угадали число!";
        }
    }
}
