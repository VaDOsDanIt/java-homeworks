package com.company.homeworks.homework6.enums;

public enum Species {
    CAT("Cat"),
    DOG("Dog");

    private String nameSpecies;

    Species(String stringSpecies) {
        this.nameSpecies = stringSpecies;
    }

    public String nameSpec() { return this.nameSpecies; };
}
