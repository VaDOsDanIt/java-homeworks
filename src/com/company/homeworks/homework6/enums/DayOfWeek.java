package com.company.homeworks.homework6.enums;

public enum DayOfWeek {
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");

    private String dayName;

    DayOfWeek(String stringDayName) {
        this.dayName = stringDayName;
    }

    public String dayName() {
        return dayName;
    }
}
